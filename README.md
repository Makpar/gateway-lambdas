# gateway-lambdas

## Sandbox Environment URLS
- Dev: [https://api-laboratory-dev.makpar-innovation.com](https://api-laboratory-dev.makpar-innovation.com)

This repository connects to the **APIGateway** and **Lamdba** components of the Innovation Lab sandbox environment on AWS.  A pipeline for delivering continuous updates to **lambda** functions is hooked into the version control. Any time a change is merged into the main environment branches, `Dev`, `Staging`, `Prod` or `Test`, the pipeline will build and deploy the changes to the AWS. The section below, [AWS Resources](/#aws-resources), details how to setup the **aws** environment through the CLI. This section should only need performed the first time this project is setup.

Note: lambda functions should be stored in a file named *lambda_function.py* and the handler function should be named *lambda_handler*. Each *lambda* function is grouped into a "component"; this allows the pipeline to determine which *lambdas* need built. Certain naming conventions need to be followed when creating new *lambdas*, to streamline the pipeline process. See [Pipeline](/#pipeline) section for more information.

## Code Quality

- [![DeepSource](https://deepsource.io/bb/Makpar/gateway-lambdas.svg/?label=active+issues&show_trend=true&token=iosjNQ4MxJ3ykJI3UCyhB6ZM)](https://deepsource.io/bb/Makpar/gateway-lambdas/?ref=repository-badge)
- [![DeepSource](https://deepsource.io/bb/Makpar/gateway-lambdas.svg/?label=resolved+issues&show_trend=true&token=iosjNQ4MxJ3ykJI3UCyhB6ZM)](https://deepsource.io/bb/Makpar/gateway-lambdas/?ref=repository-badge)

## Table of Contents

- [AWS Resource Provisioning Setup](./docs/aws_resources.md)
- [Pipeline Setup](./docs/pipeline_setup.md)
- [JIRA Integration](./docs/jira_integration.md)

## Local Setup

0. Configure **postgres** by copying the */env/.sample.env* in */env/.env* and adjusting the **postgres** variables,

```
cp .sample.env .env
```

1. Build images,

```
./scripts/docker/build-images --components alpha,beta,gamma,delta,epsilon
                              --environments Dev
```

**NOTE**: Make sure the image name and tag aligns with the tag specified in the *docker-compose.yml*, i.e. check

```
docker images
```

And verify the image name AND tag for the **Lambda** images match the *image* property in the *docker-compose.yml* for each Lambda.


2. Spin up cluster,

```
docker-compose up
```

3. Ping the servers,

```
curl -XPOST "http://localhost:8080/2015-03-31/functions/function/invocations" -d '{"payload":"hello world!"}'
curl -XPOST "http://localhost:8081/2015-03-31/functions/function/invocations" -d '{"payload":"hello world!"}'
curl -XPOST "http://localhost:8082/2015-03-31/functions/function/invocations" -d '{"payload":"hello world!"}'
curl -XPOST "http://localhost:8083/2015-03-31/functions/function/invocations" -d '{"payload":"hello world!"}'
```

## AWS Resources

Before the pipeline can deploy the lambda functions in this repository, a number of resources have to be provisioned on AWS

### Setup

The steps to provision resources on AWS necessary for this repository to function are bundled together in a series of scripts in the */scripts/* directory. In addition, the entire stack can be stood up with *CloudFormation* templates from the [DevOps/cloud-formation repository](https://bitbucket.org/Makpar/cloud-formation/src/master/)

```
./scripts/aws/iam-create-exector
./scripts/aws/ecr-create \
    --component <component-name>
./scripts/docker/build-image \
    --component <value> \
    --environment <value>
./scripts/aws/lambda-create \
    --component <value> \
    --environment <value>
./scripts/aws/iam-attach-pipeline-policies
    --components <value>,<value>
    --environment <value>,<value>
./scripts/aws/api-create
./scripts/aws/api-attach \
    --component <value>
    --endpoint <value>
    --method <value>
    --params <comma-separated-list>
```

For more detailed information, see [Detailed Steps](./docs/aws_resources.md)