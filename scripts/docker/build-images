#!/bin/bash

SCRIPT_DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
SCRIPT_NAME="build-images"
SCRIPT_DES=$'Build the image of a  \"component\" \e[5mlambda\e[0m function for a specific environment. The image repository will need an initial image pushed to it before a \e[5mlambda\e[0m function can be provisioned and associated with the image.\n\tOptions: \n\t\t--components: \e[3mRequired\e[0m. Comma separated list of the \"component\" lambdas to be built. See \e[1mREADME.md\e[0m for more information.n\t\t--environments: \e[3mRequired\e[0m. Environment branch names on which to provision the \e[5mlambda\e[0m function, e.g. \e[3mmaster\e[0m, \e[3mdevelopment\3[0m or \e[3mtest\e[0m.'
PROJECT_DIR=$SCRIPT_DIR/../..
ENV_DIR=$PROJECT_DIR/env
POLICY_DIR=$PROJECT_DIR/devops/aws/policies
source $ENV_DIR/.env


# Example Usage
# $>    build-images --components alpha,beta \
#                       --environments Prod,Dev \
#                       --action <push | build>

function log(){
    echo -e "\e[92m$(date +"%r")\e[0m: \e[4;32m$SCRIPT_NAME\e[0m : >> $1"
}

function help(){
    echo -e "\n\e[4m$SCRIPT_NAME\e[0m\n\n\t$SCRIPT_DES" 
}

while [[ $# -gt 0 ]]; do
    key="$1"
    case $key in
        --components)
            IFS=',' read -ra COMPONENTS <<< "$2"
            shift
            shift
            ;;
        --environments)
            IFS=',' read -ra ENVIRONMENTS <<< "$2"
            shift
            shift
            ;;
        --action)
            ACTION="$2"
            shift
            shift
            ;;
        --help)
            help "$SCRIPT_DES" "$SCRIPT_NAME"
            exit 0
            ;;
        *)
            log "Input not understood. See \e[3m--help\e[0m for information on using this command."
            exit 1
            ;;
    esac
done

if [ -z "$ACTION" ]
then
    ACTION="build"
fi

ECR_URI=$AWS_ACCOUNT_ID.dkr.ecr.$AWS_DEFAULT_REGION.amazonaws.com

aws ecr get-login-password --region $AWS_DEFAULT_REGION \
            | docker login --username AWS --password-stdin $ECR_URI
            
for COMPONENT in ${COMPONENTS[@]}
do 
    for ENVIRONMENT in ${ENVIRONMENTS[@]}
    do
        cd $PROJECT_DIR
        cp ./Dockerfile ./lambdas/$COMPONENT/Dockerfile
        cp -ap ./util/. ./lambdas/$COMPONENT/util/
        cd ./lambdas/$COMPONENT

        IMG_NAME=${COMPONENT_PREFIX}-${COMPONENT}

        docker build -t $IMG_NAME:$ENVIRONMENT .

        docker tag $IMG_NAME:$ENVIRONMENT $ECR_URI/$IMG_NAME:$ENVIRONMENT

        if [ "$ACTION" == "push" ]
        then
            docker push $ECR_URI/$IMG_NAME:$ENVIRONMENT
        fi

        rm ./Dockerfile
        rm -r ./util/*
        rm -r ./util
    done
done
