"""
# Lambda Beta

A lambda code scaffolding for an RDS query pre-configured for quick deployment.
"""

from psycopg2 import OperationalError, DatabaseError
from typing import Any, Dict, Union
import json
import os
import sys
sys.path.append(os.path.dirname(__file__))
from util import logger, rds, validator


dbname = str(os.environ.setdefault(
    "POSTGRES_DBNAME", 'test').strip("\"").strip("'"))
"""
RDS database name delivered to **Lambda** function from CloudFormation (or pipeline testing executor) into environment variable, *POSTGRES_DBNAME*

.. notes::
    * This database does not exist in the RDS until initialized.
"""
dbschema = str(os.environ.setdefault("POSTGRES_SCHEMA", 'postgres')).strip("'")
"""
RDS schema name delivered to **Lambda** function from CloudFormation or (pipeline testing exectuor) into environment variable, **POSTGHRES_SCHEMA**.
"""
tablename = str(os.environ.setdefault(
    "POSTGRES_TABLE", 'mock_data')).strip("'")

"""
RDS tablename delivered to **Lambda** function from CloudFormation or (pipeline testing exectuor) into environment variable, **POSTGHRES_TABLE**.
"""
# QUERIES
QUERY = "SELECT * FROM {schema}.{table} WHERE beta_param=%(beta_param)s;"
"""Query used to populate response"""

log = logger.getLogger(__name__)


def lambda_handler(event: Dict[str, Any], context: Any) -> Dict[str, Union[int, str, Dict[str, Any]]]:
    """
    Description goes here

    Parameters
    ----------
    1. event: Dict[str, Any]
        * event['HttpMethod'] : Method of the request.
        * event['pathParameters']['*'] : Path parameters explanation
        * event['body']['*'] : Body parameters explanation
    2. context: Any
        Dictionary containing information about the Lambda runtime.

    Returns
    -------
    Dict[str, Union[int, str, Dict[str, Any]]]
        ```
            {
            "isBase64Encoded": False,
            "statusCode": 200 | 500,
            "headers": {
                "Access-Control-Allow-Headers" : "Content-Type",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Methods": "OPTIONS,POST,GET",
                "Content-Type": "application/json"
            },
            "body": `str`
        }
        ```

        The body will contain a stringified JSON which API Gateway will transform into a full-fledge JSON in the following format,

        ```
        {
            "results": [
                {   
                    "label" : str ,
                    "value": str
                }
            ]
        }
        ```
    """
    log.info('Received event with attributes:')
    for key, value in event.items():
        log.info('%s : %s', key, value)

    pathParameterKey = 'beta_param'
    body = {}

    if validator.validate_event_params(event, pathParameterKey):
        statusCode = 200
        query_params = {
            pathParameterKey: event['pathParameters'][pathParameterKey],
        }
        try:
            results = rds.query_to_dict(
                dbname, dbschema, tablename, QUERY, query_params)
            body = {
                "results": [
                    {
                        'label': str(value[0]),
                        'value': str(value[0])
                    }
                    if value[0] else
                    {
                        'label': None,
                        'value': None
                    }
                    for value in results.values()
                ]
            }
            log.info('Returning Result: %s', body)
        except (OperationalError, DatabaseError) as e:
            log.error(e)
            statusCode = 500
            body = {'message': f'Failure: {e}'}

    else:
        statusCode = 400
        body = {'message': 'Bad Request'}

    return {
        "isBase64Encoded": False,
        "statusCode": statusCode,
        "headers": {
            "Access-Control-Allow-Headers": "Content-Type",
            "Allow": "GET, OPTIONS, POST",
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Methods": "OPTIONS,POST,GET",
            "Content-Type": "application/json"
        },
        "body": json.dumps(body)
    }


if __name__ == "__main__":
    print(lambda_handler(None, None))
