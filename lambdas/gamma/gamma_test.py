import pytest
import json
import os

from .lambda_function import lambda_handler

def test_body_data_type():
    """
    Test data type of response body.
    """
    event = {}
    response = lambda_handler(event, None)
    assert(isinstance(response['body'], str))


def test_allow_header():
    """
    Test that the `Allow` header is present in response.
    """
    event = {}
    response = lambda_handler(event, None)
    assert('Allow' in response['headers'].keys()
           and 'GET' in response['headers']['Allow']
           and 'POST' in response['headers']['Allow']
           and 'OPTIONS' in response['headers']['Allow']
           )


def test_cors_access_headers():
    """
    Test that the `Access-Control-Allow-Headers` header is present in response headers.
    """
    event = {}
    response = lambda_handler(event, None)
    assert('Access-Control-Allow-Headers' in response['headers'].keys()
           and 'Content-Type' in response['headers']['Access-Control-Allow-Headers']
           )


def test_cors_access_origin():
    """
    Test that the `Access-Control-Allow-Origin` header is present in response headers.
    """
    event = {}
    response = lambda_handler(event, None)
    assert('Access-Control-Allow-Origin' in response['headers'].keys())


def test_cors_access_methods():
    """
    Test that the `Access-Control-Allow-Methods` header is present and allows `GET`, `POST` and `OPTIONS` methods.
    """
    event = {}
    response = lambda_handler(event, None)
    assert('Access-Control-Allow-Methods' in response['headers'].keys()
           and 'GET' in response['headers']['Access-Control-Allow-Methods']
           and 'POST' in response['headers']['Access-Control-Allow-Methods']
           and 'OPTIONS' in response['headers']['Access-Control-Allow-Methods']
           )


def test_content_type():
    """
    Test that the `Content-Type` header is present and set equal to `application/json`.
    """
    event = {}
    response = lambda_handler(event, None)
    assert('Content-Type' in response['headers'].keys()
           and 'application/json' == response['headers']['Content-Type'])
