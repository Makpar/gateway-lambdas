"""
# Lambda Gamma

A lambda code scaffolding pre-configured for quick deployment with mock data.
"""

import json
from typing import Any, Dict, Union
import os
import sys
import requests
import urllib.parse
sys.path.append(os.path.dirname(__file__))
from util import logger

log = logger.getLogger(__name__)

def lambda_handler(event: Dict[str, Any], context: Any) -> Dict[str, Union[int, str, Dict[str, Any]]]:
    """
    Function description goes here

    Parameters
    ----------
    1. event: Dict[str, Any]
        * event['HttpMethod'] : Method of the request.
        * event['pathParameters']['*'] : Path parameters explanation
        * event['body']['*'] : Body parameters explanation
    2. context: Any
        Dictionary containing information about the Lambda runtime.

    Returns
    -------
    Dict[str, Union[int, str, Dict[str, Any]]]
        ```
            {
            "isBase64Encoded": False,
            "statusCode": 200 | 500,
            "headers": {
                "Access-Control-Allow-Headers" : "Content-Type",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Methods": "OPTIONS,POST,GET",
                "Content-Type": "application/json"
            },
            "body": `str`
        }
        ```

        The body will contain a stringified JSON which API Gateway will transform into a full-fledge JSON in the following format,

        ```
        {
            "results": [
                {   
                    RESULT FORMAT GOES HERE
                }
            ]
        }
        ```
    """

    log.info('Received event with attributes:')
    for key, value in event.items():
        log.info('%s : %s', key, value)

    body_check = event.get('body', None)
    if body_check is not None:
        
        event_body = json.loads(event['body'])
        # get body params
        # r = requests.get(f'url')
        body = { 'message': 'Hello From Lambda'}

    else:
        statusCode = 400
        body = {"message": "Bad Request; No body"}


   

    return {
        "isBase64Encoded": False,
        "statusCode": statusCode,
        "headers": {
            "Access-Control-Allow-Headers": "Content-Type",
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Methods": "OPTIONS,POST,GET",
            "Allow": "GET, OPTIONS, POST",
            "Content-Type": "application/json"
        },
        "body": json.dumps(body)
    }


if __name__ == "__main__":
    print(lambda_handler(None, None))
