# aws sdk
boto3==1.18.51
psycopg2-binary==2.9.1
sqlalchemy==1.4.26
requests==2.26.0
pandas==1.3.4