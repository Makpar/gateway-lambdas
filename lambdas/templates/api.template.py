import json
import os
import requests
import pandas as pd
from typing import Any, Dict, Union

env = os.environ.setdefault('ENV', "lambda")

if env == "lambda":
    from util import logger, validator
else:
    from . import logger, validator

API_KEY = os.getenv('API_KEY')

log = logger.getLogger(__name__)


def lambda_handler(event: Dict[str, Any], context: Any) -> Dict[str, Union[int, str, Dict[str, Any]]]:
    """
    Function description goes here

    Parameters
    ----------
    1. event: Dict[str, Any]
        * event['HttpMethod'] : Method of the request.
        * event['pathParameters']['*'] : Path parameters explanation
        * event['body']['*'] : Body parameters explanation
    2. context: Any
        Some mysterious Lambda construct.

    Returns
    -------
    Dict[str, Union[int, str, Dict[str, Any]]]
        ```
            {
            "isBase64Encoded": False,
            "statusCode": 200 | 500,
            "headers": {
                "Access-Control-Allow-Headers" : "Content-Type",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Methods": "OPTIONS,POST,GET",
                "Content-Type": "application/json"
            },
            "body": `str`
        }
        ```

        The body will contain a stringified JSON which API Gateway will transform into a full-fledge JSON in the following format,

        ```
        {
            "results": [
                {   
                    RESULT FORMAT GOES HERE
                }
            ]
        }
        ```
    """

    log.info('Received event with attributes:')
    for key, value in event.items():
        log.info('%s : %s', key, value)

    pathParameterKey = 'CHANGEME'

    if validator.validate_event_params(event, pathParameterKey):
        statusCode = 200

        try:
            r = requests.get(f"URL GOES HERE with {API_KEY}")
            data = r.json()
            df = pd.DataFrame.from_dict(data['Results'])
            df1 = df[['COLUMN NAMES GO HERE']]
            statusCode = 200
            body = df1.DataFrame.to_dict()
        except Exception as e:
            statusCode = 500
            body = {"message": f'Failure: {e}'}

    else:
        statusCode = 400
        body = {'message': 'Bad Request'}

    return {
        "isBase64Encoded": False,
        "statusCode": statusCode,
        "headers": {
            "Access-Control-Allow-Headers": "Content-Type",
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Methods": "OPTIONS,POST,GET",
            "Allow": "GET, OPTIONS, POST",
            "Content-Type": "application/json"
        },
        "body": json.dumps(body)
    }


if __name__ == "__main__":
    print(lambda_handler(None, None))
