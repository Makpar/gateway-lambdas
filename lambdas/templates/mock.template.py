"""
# Lambda Delta

A lambda code scaffolding pre-configured for quick deployment.
"""
import os
import json
from typing import Any, Dict, Union

env = os.environ.setdefault('ENV', "lambda")

if env == "lambda":
    from util import logger
else:
    from . import logger


log = logger.getLogger(__name__)


def lambda_handler(event: Dict[str, Any], context: Any) -> Dict[str, Union[int, str, Dict[str, Any]]]:

    mockData = ["Peter", "Thomas", "Phung", "Grant", "Matt",
                "MattTheSequel", "Ian", "Tariq", "Mary-Anne", "Mike"]
    body = {
        "results": [
            {
                "label": "team_member",
                "value": mockDatum
            } for mockDatum in mockData

        ]
    }

    return {
        "isBase64Encoded": False,
        "statusCode": 200,
        "headers": {
            "Access-Control-Allow-Headers": "Content-Type",
            "Allow": "GET, OPTIONS, POST",
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Methods": "OPTIONS,POST,GET",
            "Content-Type": "application/json"
        },
        "body": json.dumps(body)
    }


if __name__ == "__main__":
    print(lambda_handler(None, None))
