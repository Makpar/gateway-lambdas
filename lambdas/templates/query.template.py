import json
import os
from typing import Any, Dict, Union
from psycopg2 import OperationalError, DatabaseError

env = os.environ.setdefault('ENV', "lambda")

if env == "lambda":
    from util import logger, rds, validator
else:
    from . import logger, rds, validator

dbname = str(os.getenv("POSTGRES_DBNAME")).strip('"').strip("'")
"""
RDS database name delivered to **Lambda** function from CloudFormation into environment variable, *POSTGRES_DBNAME*

.. notes::
    * This database does not exist in the RDS until initialized.
"""

# QUERIES
QUERY = "SELECT * FROM {}.!!!REPLACE_WITH_TABLE_NAME!!!;"
"""Query used to populate response"""


def validate_event_params(event: Dict[str, Any], path_arg: str) -> bool:
    """
    A function for determining if the `pathParameters` attributes is present and not null in the `event` dictionary.

    Parameters
    ----------
    1. event: Dict[str, Any]
        Event passed into Lambda
    2. path_arg: str
        Name of the path parameter the function is validating against.

    Returns
    -------
    bool
        `true` if `path_arg` is present in `event['pathParameters']`, `false` otherwise.

    """
    log.info('Validing event parameters')
    if 'pathParameters' in event.keys() and event['pathParameters'] is not None:
        log.info('pathParameters in event and not null')
        if path_arg in event['pathParameters'].keys() and event['pathParameters'][path_arg] is not None:
            log.info(f'{path_arg} in pathParameters and not null')
            return True
    return False


log = logger.getLogger(__name__)


def lambda_handler(event: Dict[str, Any], context: Any) -> Dict[str, Union[int, str, Dict[str, Any]]]:
    """
    Description Goes Here

    Parameters
    ----------
    1. event: Dict[str, Any]
        * event['HttpMethod'] : Method of the request.
        * event['pathParameters']['*'] : Path parameters explanation
        * event['body']['*'] : Body parameters explanation
    2. context: Any
        Some mysterious Lambda construct.

    Returns
    -------
    Dict[str, Union[int, str, Dict[str, Any]]]
        ```
            {
            "isBase64Encoded": False,
            "statusCode": 200 | 500,
            "headers": {
                "Access-Control-Allow-Headers" : "Content-Type",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Methods": "OPTIONS,POST,GET",
                "Content-Type": "application/json"
            },
            "body": `str`
        }
        ```

        The body will contain a stringified JSON which API Gateway will transform into a full-fledge JSON in the following format,

        ```
        {
            "results": [
                {   
                    RESULT FORMAT GOES HERE
                }
            ]
        }
        ```
    """
    log.info('Received event with attributes:')
    for key, value in event.items():
        log.info('%s : %s', key, value)

    pathParameterKey = 'REPLACEME'
    body = {}

    if validator.validate_event_params(event, pathParameterKey):
        statusCode = 200
        query_params = {'param1': 0, 'param2': 1}
        dbschema = 'ccc'
        try:
            results = rds.query_to_dict(dbname, dbschema, QUERY, query_params)
            body = {
                "results": [
                    {
                        'label': str(value[0]),
                        'value': str(value[0])
                    }
                    if value[0] else
                    {
                        'label': None,
                        'value': None
                    }
                    for value in results.values()
                ]
            }
        except (OperationalError, DatabaseError) as e:
            log.error(e)
            statusCode = 500
            body = {'message': f'Failure: {e}'}

    else:
        statusCode = 400
        body = {'message': 'Bad Request'}

    return {
        "isBase64Encoded": False,
        "statusCode": statusCode,
        "headers": {
            "Access-Control-Allow-Headers": "Content-Type",
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Methods": "OPTIONS,POST,GET",
            "Allow": "GET, OPTIONS, POST",
            "Content-Type": "application/json"
        },
        "body": json.dumps(body)
    }


if __name__ == "__main__":
    print(lambda_handler(None, None))
