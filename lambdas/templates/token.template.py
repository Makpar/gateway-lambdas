import os
import json
import boto3
from typing import Any, Dict, Union

CLIENT_ID = os.getenv('CLIENT_ID')
"""Cognito API Client ID"""
POOL_ID = os.getenv('POOL_ID')
"""Cognito User Pool ID"""


def lambda_handler(event: Dict[str, Any], context: Any) -> Dict[str, Union[int, str, Dict[str, Any]]]:
    """    
    Returns Access, Refresh and ID token for a given user from the Cognito client.

    Parameters
    ----------
    1. event['username'] : str
        Username/email of user retrieving token
    2. event['password']: str
        Password of the user retrieving tokens

    Returns
    -------
    Nested dictionary with tokens stored in `body`. Response gets transformed through API gateway, so that the response body will be formatted as,

    ```
    {
        "ChallengeParameters": "",
        "AuthenticationResults": {
            "AccessToken": "",
            "ExpiresIn": "",
            "TokenType": "",
            "RefreshToken: "",
            "IdToken": "",
        }
    }
    ``` 
    """
    try:
        client = boto3.client('cognito-idp', region_name='us-east-1')
        response = client.initiate_auth(
            ClientId=CLIENT_ID,
            AuthFlow='USER_PASSWORD_AUTH',
            AuthParameters={
                'USERNAME': json.loads(event['body'])['username'],
                'PASSWORD': json.loads(event['body'])['password']
            }
        )
    except Exception as e:
        return {
            "isBase64Encoded": False,
            "statusCode": 200,
            "headers": {'Content-Type': 'application/json'},
            "body": f'failure {e}'
        }

    return {
        "isBase64Encoded": False,
        "statusCode": 200,
        "headers": {
            "Access-Control-Allow-Headers": "Content-Type",
            "Allow": "GET, OPTIONS, POST",
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Methods": "OPTIONS,POST,GET",
            "Content-Type": "application/json"
        },
        "body": json.dumps(response)
    }


if __name__ == "__main__":
    print(lambda_handler(None, None))
