"""
# Lambda Alpha Tests

Unit tests for Lambda Alpha. Lambda Alpha is set up to accept a path parameter in the request url. The request url is parsed at the API Gateway level and in turn posted to the Lambda. Lambda receives the path parameter in the `event` dictionary. 

These tests are run in the CI/CD pipeline where a live Postgres instance is spun up and populated with the data in */tests/mock_data.csv*. The tests are parameterized for success and failtures against the values in this spreadsheet.
"""
import json
import pytest


from .lambda_function import lambda_handler

@pytest.mark.parametrize(["path_param", "result"], [("a", 1),("b", 1), ("c", 1)])
def test_query_with_valid_path_and_valid_arg(path_param, result):
    """
    Test simple RDS query against mock data and validate number of results returned.
    """
    event = {
        'pathParameters': {
            'alpha_param': path_param
        }
    }
    response = lambda_handler(event, None)
    assert(len(json.loads(response['body'])['results']) == result)

@pytest.mark.parametrize(["path_param", "status_code"], [("a", 200),("b", 200), ("c", 200)])
def test_status_code_with_valid_path_and_valid_arg(path_param, status_code):
    """
    Test simple RDS query against mock data and validate status code returned.
    """
    event = {
        'pathParameters': {
            'alpha_param': path_param
        }
    }
    response = lambda_handler(event, None)
    assert(response['statusCode'] == status_code)

@pytest.mark.parametrize(["path_param", "status_code"], [("x", 404),("y", 404), ("z", 404)])
def test_status_code_with_valid_path_and_invalid_arg(path_param, status_code):
    """
    Test simple RDS query against bad mock data and validate status codes.
    """
    event = {
        'pathParameters': {
            'alpha_param': path_param
        }
    }
    response = lambda_handler(event, None)
    assert(response['statusCode'] == status_code)

def test_status_code_with_only_path_param():
    """
    Test how Lambda that is expecting a path parameter handles null path parameter value and validate status code returned.
    """
    event = {
        'pathParameters': {
            'alpha_param': None
        }
    }
    response = lambda_handler(event, None)
    assert(response['statusCode'] == 400)

def test_status_code_with_invalid_path_and_valid_arg():
    """
    Test how Lambda handles a path parameter it does not recognize and validate status code returned.
    """
    event = {
        'pathParameters': {
            'not_alpha_param': 'a'
        }
    }
    response = lambda_handler(event, None)
    assert(response['statusCode'] == 400)


def test_status_code_with_null_path_param():
    """
    Test how Lambda that is expecting path parameter handles null path paramter dictionary and validate status code returned.
    """
    event = {
        'pathParameters': {}
    }
    response = lambda_handler(event, None)
    assert(response['statusCode'] == 400)


def test_body_data_type():
    """
    Test data type of response body.
    """
    event = {}
    response = lambda_handler(event, None)
    assert(isinstance(response['body'], str))


def test_allow_header():
    """
    Test that the `Allow` header is present in response.
    """
    event = {}
    response = lambda_handler(event, None)
    assert('Allow' in response['headers'].keys()
           and 'GET' in response['headers']['Allow']
           and 'POST' in response['headers']['Allow']
           and 'OPTIONS' in response['headers']['Allow']
           )


def test_cors_access_headers():
    """
    Test that the `Access-Control-Allow-Headers` header is present in response headers.
    """
    event = {}
    response = lambda_handler(event, None)
    assert('Access-Control-Allow-Headers' in response['headers'].keys()
           and 'Content-Type' in response['headers']['Access-Control-Allow-Headers']
           )


def test_cors_access_origin():
    """
    Test that the `Access-Control-Allow-Origin` header is present in response headers.
    """
    event = {}
    response = lambda_handler(event, None)
    assert('Access-Control-Allow-Origin' in response['headers'].keys())


def test_cors_access_methods():
    """
    Test that the `Access-Control-Allow-Methods` header is present and allows `GET`, `POST` and `OPTIONS` methods.
    """
    event = {}
    response = lambda_handler(event, None)
    assert('Access-Control-Allow-Methods' in response['headers'].keys()
           and 'GET' in response['headers']['Access-Control-Allow-Methods']
           and 'POST' in response['headers']['Access-Control-Allow-Methods']
           and 'OPTIONS' in response['headers']['Access-Control-Allow-Methods']
           )


def test_content_type():
    """
    Test that the `Content-Type` header is present and set equal to `application/json`.
    """
    event = {}
    response = lambda_handler(event, None)
    assert('Content-Type' in response['headers'].keys()
           and 'application/json' == response['headers']['Content-Type'])
