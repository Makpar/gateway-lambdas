"""
# Lambda Delta

A lambda code scaffolding pre-configured for quick deployment.
"""

import json
from typing import Any, Dict, Union
import os
import sys
sys.path.append(os.path.dirname(__file__))
from util import logger

log = logger.getLogger(__name__)

def lambda_handler(event: Dict[str, Any], context: Any) -> Dict[str, Union[int, str, Dict[str, Any]]]:
    """
    A Lambda function to return mock data, so the front-end has something to talk to as soon as development begins.

    Parameters
    ----------
    1. event: Dict[str, Any]
        * event['HttpMethod'] : Method of the request.
        * event['pathParameters']['*'] : Path parameters explanation
        * event['body']['*'] : Body parameters explanation
    2. context: Any
        Dictionary containing information about the Lambda runtime.

    Returns
    -------
    Dict[str, Union[int, str, Dict[str, Any]]]
        ```
            {
            "isBase64Encoded": False,
            "statusCode": 200 | 500,
            "headers": {
                "Access-Control-Allow-Headers" : "Content-Type",
                "Access-Control-Allow-Origin": "*",
                "Access-Control-Allow-Methods": "OPTIONS,POST,GET",
                "Content-Type": "application/json"
            },
            "body": `str`
        }
        ```

        The body will contain a stringified JSON which API Gateway will transform into a full-fledge JSON in the following format,

        ```
        {
            "results": [
                {   
                    "label" : str ,
                    "value": str
                }
            ]
        }
        ```
    """

    mockData = ["Peter", "Thomas", "Phung", "Grant", "Matt",
                "MattTheSequel", "Ian", "Tariq", "Mary-Anne", "Mike"]
    body = {
        "results": [
            {
                "label": "team_member",
                "value": mockDatum
           } for mockDatum in mockData

        ]
    }

    return {
        "isBase64Encoded": False,
        "statusCode": 200,
        "headers": {
            "Access-Control-Allow-Headers": "Content-Type",
            "Allow": "GET, OPTIONS, POST",
            "Access-Control-Allow-Origin": "*",
            "Access-Control-Allow-Methods": "OPTIONS,POST,GET",
            "Content-Type": "application/json"
        },
        "body": json.dumps(body)
    }


if __name__ == "__main__":
    print(lambda_handler(None, None))
