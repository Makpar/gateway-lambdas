import logging


def getLogger(name: str) -> logging.Logger:
    """
    Returns an instance of the `logging.Logger` class configured to print events on the `logging.INFO` level to `stdout`.
    """
    logFormatter = logging.Formatter(
        "%(asctime)s [%(threadName)-12.12s] [%(levelname)-5.5s]  %(message)s")
    logger = logging.getLogger(name)
    consoleHandler = logging.StreamHandler()
    consoleHandler.setFormatter(logFormatter)
    logger.setLevel(logging.INFO)
    logger.addHandler(consoleHandler)
    return logger
