from typing import Any, Dict
import os
import sys
sys.path.append(os.path.dirname(__file__))
import logger

log = logger.getLogger(__name__)


def validate_event_params(event: Dict[str, Any], path_arg: str) -> bool:
    """
    A function for determining if the `pathParameters` attributes is present and not null in the `event` dictionary.

    Parameters
    ----------
    1. event: Dict[str, Any]
        Event passed into Lambda
    2. path_arg: str
        Name of the path parameter the function is validating against.

    Returns
    -------
    bool
        `true` if `path_arg` is present in `event['pathParameters']`, `false` otherwise.

    """
    log.info('Validing event parameters')
    if 'pathParameters' in event.keys() and event['pathParameters'] is not None:
        log.info('pathParameters in event and not null')
        if path_arg in event['pathParameters'].keys() and event['pathParameters'][path_arg] is not None:
            log.info(f'{path_arg} in pathParameters and not null')
            return True
    return False
