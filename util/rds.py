"""
# RDS

Module containing functions for interacting with the RDS service.
"""
from psycopg2.extensions import ISOLATION_LEVEL_AUTOCOMMIT
from psycopg2._psycopg import connection
from psycopg2 import connect, sql
from typing import Any, Dict, List, Tuple
import zipfile
import os
import sys
sys.path.append(os.path.dirname(__file__))
import logger

log = logger.getLogger(__name__)

# SERVICE CONFIGURATION
connection_config = {
    'host': os.getenv("POSTGRES_HOST"),
    'password': os.getenv("POSTGRES_PASSWORD"),
    'user': os.getenv("POSTGRES_USER")
}
"""
Dictionary containg AWS Postgres RDS credentials delivered through the environment variables, *POSTGRES_HOST*, *POSTGRES_PASSWORD* and *POSTGRES_USER*.
"""


def get_connection(dbname: str) -> connection:
    """
    Uses the environment connection configuration to establish a connection with the database `dbname` hosted on the RDS service.

    Parameters
    ----------
    1. dbname: ``str``
        Name of the database to connect to.

    Returns
    -------
    `psycopg2.connection`
        A connection to the RDS service with the isolation level set to `psycopg2.extensions.ISOLATION_LEVEL_AUTOCOMMIT`, in order to allow transaction to be commited to the database.
    """
    log.info('Connecting to Database: %s', dbname)
    log.info('%s', connection_config)
    conn = connect(dbname=dbname,
                   user=connection_config['user'],
                   password=connection_config['password'],
                   host=connection_config['host'],
                   port=5432)
    conn.set_isolation_level(ISOLATION_LEVEL_AUTOCOMMIT)
    return conn


def query_to_dict(dbname: str, dbSchema: str, dbTable: str, query: str, params: Dict[str, Any] = None) -> Dict[int, List]:
    """
    Executes `query` against the `dbSchema` contained in the `dbname` database. If the query is parameterized, the query parameters can be passed in through the optional `params` argument. The query result is formatted into a dictionary where each key-value pair contains an array containing the rows of the query result.

    Parameters
    ----------
    1. dbname : ``str``
        Name of the database against which to run the query.
    2. dbSchema: ``str``
        Name of the database schema against which to run the query.
    3. dbTable: ``str``
        Name of the database table against which to run the query
    3. query: ``str``
        A SQL query formatted according to the rules of `psycopg2`, i.e. parameters should be parameterized with the %(name)s syntax.
    4. params: ``str``
        *Optional*. A dictionary of the parameters to injected into `query`. Keys in the dictionary must match the name of the parameters in the SQL query. Defaults to `None`, i.e. assumes `query` is *not* parameterized.

    Returns
    -------
    dict
        ```
        {
            0: list,
            1: list,
            ...
        }
        ```

    Examples
    --------

    If the query `SELECT * FROM table WHERE col=%(name)` is to be executed against the schema `schema` in the database `db` with the paramaters `name=myCol`, this function is called as follows,

        ```
        query_to_dict('db','schema', 'SELECT * FROM table WHERE col=%(name)s', { 'name' : 'myCol' })
        ```
    """
    body = {}
    conn = get_connection(dbname)
    with conn.cursor() as cur:
        log.info('Executing query: %s', query)
        if params is not None:
            cur.execute(
                sql.SQL(query).format(schema=sql.Identifier(dbSchema),
                                      table=sql.Identifier(dbTable)),
                params
            )
        else:
            cur.execute(
                sql.SQL(query).format(schema=sql.Identifier(dbSchema),
                                      table=sql.Identifier(dbTable))
            )

        for i, item in enumerate(cur):
            body[i] = item
    conn.close()
    return body


def get_sql_alchemy_connection(dbName: str) -> Tuple[Any, Any]:
    """
    Returns a SQLAchemy (connection, engine)-tuple for the database `dbName` using the RDS credentials provisioned in the environment.

    Parameters
    ----------
    1. dbName: ``str``
        Name of the database.
    """
    from sqlalchemy import create_engine

    engine = create_engine(
        f'postgresql://{connection_config["user"]}:{connection_config["password"]}@{connection_config["host"]}:5432/{dbName}')
    con = engine.connect()
    return con, engine


def fileToSql(filename: str, dbSchema: str, dbName: str, zipped: bool = False) -> None:
    """
    Converts the csv file `filename` in the current working directory into a Postgres schema.

    Parameters
    ----------
    1. filename: ``str``
        Name of the csv file in the current working directory.
    2. dbSchema: ``str``
        Name of the schema to be created.
    3. dbName: ``str``
        Name of the database to connect to.
    4. zipped: ``bool``
        *Optional*. Flag indictating whether or not `filename` is zipped. Defaults to `false`
    """
    from sqlalchemy import schema
    import pandas as pd

    if zipped:
        with zipfile.ZipFile(filename, 'r') as zipRef:
            zipRef.extractall(os.getcwd())

    con, engine = get_sql_alchemy_connection(dbName)

    if not con.dialect.has_schema(engine, dbSchema):
        con.execute(schema.CreateSchema(dbSchema))

    for file in os.listdir(os.getcwd()):
        print(file)
        if file.endswith('.csv'):
            df = pd.read_csv(os.path.join(os.getcwd(), file))
            dbTableName = os.path.splitext(file)[0].lower()
            df.to_sql(dbTableName, con=engine,
                      schema=dbSchema, if_exists='replace')

    con.close()
