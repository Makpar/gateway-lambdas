FROM public.ecr.aws/lambda/python:3.8

# OS Dependencies
# Necessary dependencies if using psycopg2 (not psycopg2-binary!)
# RUN yum update -y && yum install -y postgresql-devel gcc gcc-c++ kernel-devel make

# Application Dependencies
COPY requirements.txt  .
RUN  pip3 install -r requirements.txt --target "${LAMBDA_TASK_ROOT}"

# Copy function code into image
COPY . ${LAMBDA_TASK_ROOT}

# Set the CMD to your handler (could also be done as a parameter override outside of the Dockerfile)
CMD [ "lambda_function.lambda_handler" ]