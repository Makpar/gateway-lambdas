# Pipeline

The pipeline containerizes **lambda** functions in the sub-directories through the *Dockerfile* in the project root. Project dependencies are injected into the *Dockerfile* through the *requirements.txt*; this template only installs [boto3](https://boto3.amazonaws.com/v1/documentation/api/latest/index.html), a python AWS SDK, by default. The pipeline will build the **lambda** function images, push them to an **ECR** repository, and then deploy to **lambda** anytime commits are merged onto one of the main environment branches: `test`, `development` or `master`.

The pipeline splits the **lambdas** into "components", represented by the different directories in this project. For the purposes of the pipeline, the **lambda** functions should be stored inside of these directories in a file named *lambda_function.py* and the handler function should be named *lambda_handler*. Adopting this naming convention decreases the amount of scripting needed in the pipeline in order to automate the deployment. In other words, the pipeline doesn't care about the content of the functions, only that they all require the same deployment steps. The pipeline only distinguishes between the **lambdas** on the level of the "component". 

If a new **lambda** function is added to the project, in order to get it hooked into the pipeline, following the structure of the **lambda** directories as a template, perform these steps:

- create a directory to hold the function, */lambda-<component-name>/*. NAMING CONVENTION IS IMPORTANT OR ELSE PIPELINE WON'T WORK.
- add a *requirements.txt* file to declare your dependencies
- add a function script named *lambda_function.py* with a function named *lambda_hanlder*.
- in the *bitbucket-pipelines.yml*, add a step to each pipeline workflow to deploy your new "component".

## Setup

1. Add the following deployment variables through **Repository Settings > Deployments**: **AWS_ACCOUNT_ID**, **AWS_ACCESS_KEY_ID**, **AWS_SECRET_ACCESS_KEY**, **AWS_DEFAULT_REGION**, **LAMBDA_IMG_NAME** and **LAMBDA_FUNC_NAME**. The first four variables are standard AWS creds. We might consider provisioning separate accounts for interacting with specific environment, i.e. one account only has permission to update lambda functions on the test environment, one account only has permission to update on staging, etc. This way we isolate our resources against possible intrusions; if one set of keys gets exposed, the other environments are not comprised.

The last two variables, **LAMBDA_IMG_NAME** and **LAMBDA_FUNC_NAME**, correpond to the image repository name and function name given to the **ECR** repository and **Lambda** function provisioned in the first section [AWS Resources](#aws-resources)

This can be automated with BitBucket's API.

2. Make sure the *bitbucket-pipelines.yml* from the */devops/* folder is in the project root. Initialize the pipeline either through the UI or API. The pipeline should kick off after that!
