# AWS Resource Provisioning Detailed Steps

## Prerequisites

**Required**

- [aws cli](https://aws.amazon.com/cli/)
- [docker](https://www.docker.com/products/docker-desktop)

**Optional**

You will need to parse a JSON to attach a **lambda** function to an **apigateway**, in order to get resource IDs. I used the CLI utility `jq`.

- [jq](https://stedolan.github.io/jq/download/)

## Steps

Configuration is stored in an environment file. Modify */env/.env/* for your specific environment, and then load the environment file into your shell. In addition, ensure your **aws** CLI has been authenticated,

```
source /env/.env
aws configure
```

Finally, log into **aws ecr**. Note, the environment variables loaded into your shell session get substituted into the following command automatically if you sourced the environment file, 

```
aws ecr get-login-password --region $AWS_REGION | docker login --username AWS --password-stdin $AWS_ACCOUNT_ID.dkr.ecr.$AWS_REGION.amazonaws.com

```
1. Provision **ECR** repo for **lambda** container image,

```
aws ecr create-repository \
    --repository-name <component> \
    --image-scanning-configuration scanOnPush=true \
    --region $AWS_DEFAULT_REGION
```

2. Attach policies to **ECR** to allow **lambda** to pull images,

```
aws set-repository-policy \
    --registry-id $AWS_ACCOUNT_ID \
    --repository-name <component> \
    --policy-text file://ecr.json
```

NOTE #1: For some reason, the policy json has to be in the same directory as the directory you execute this command. Be sure to copy the */devops/aws/policies/ecr.json* into your current working directory before executing this command.

NOTE #2: Need ECR for each "component", so Steps #1 & #2 should be repeated for each "component" **lambda**.

3. Build and push **lambda** image to **ECR**,

```
docker build -t <component>:<environment> .
docker tag <component>:<environment>  $AWS_ACCOUNT_ID.dkr.ecr.$AWS_REGION.amazonaws.com/<component>:<environment> 
docker push
```

NOTE #1: Need image for each component and environment, i.e. *lambda-alpha:master*, *lambda-alpha:development*, ... , *lambda-beta:master*, *lambda-beta:development*, ... Multiply number of environments by number of components to determine total number of images necessary.

4. Create **lambda** execution role and attach minimal policies,

```
aws iam create-role --role-name $LAMBDA_ROLE --assume-role-policy-document file://lambda.json
aws iam attach-role-policy --role-name $LAMBDA_ROLE --policy-arn arn:aws:iam::aws:policy/service-role/AWSLambdaBasicExecutionRole
```
NOTE: For some reason, the policy json has to be in the same directory as the directory you execute this command. Be sure to copy the */devops/aws/policies/lambda.json* into your current working directory before executing this command.

5. Create **lambda** functions,

```
aws lambda create-function --region $AWS_DEFAULT_REGION --function-name $FUNC_NAME \
    --package-type Image  \
    --code ImageUri=$AWS_ACCOUNT_ID.dkr.ecr.$AWS_REGION.amazonaws.com/$IMG_NAME:$IMG_TAG   \
    --role arn:aws:iam::$AWS_ACCOUNT_ID:role/$LAMBDA_ROLE
```

NOTE #1: Need function for each component and environment, i.e. *lambda-alpha:master*, *lambda-alpha:development*, ... , *lambda-beta:master*, *lambda-beta:development*, ... Multiply number of environments by number of components to determine total number of functions necessary.

6. Provision API Gateway REST API

```
aws apigateway create-rest-api --name $GATEWAY_NAME --region $AWS_DEFAULT_REGION
```

7. TODO: create pipeline user on aws, attach policies in */devops/aws/policies/pipeline.json*. We already have a dummy account on AWS for our **BitBucket** pipeline with the correct policies attached, so I'll detail this step later. You will need the access key and secret access key for this dummy account for what follows.

