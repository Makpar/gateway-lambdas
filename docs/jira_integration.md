# Jira Integration

**Jira** has a feature where it will hook into our repository and associate commits with tasks/stories/epics if we follow a **Jira** naming and formatting convention. See [documentation](https://support.atlassian.com/bitbucket-cloud/docs/use-smart-commits/) for more information. The important bit is your branch or commit must be addressed to the issue it is tracking; You need to include the issue key in the branch name and commit message. You can find the issue key under *Projects > Issues > All Issues*,

![Jira Issue Key](./assets/jira_issue_key.png)

In the above example, *CCC-3* is the story issue key and a *CCC-4* is a task issue key associated with that story. Therefore, I create a branch with the following name

```
git checkout -b CCC-3/pipeline
```

Then, I will add my code, check it in and add a commit message in the following way,

```
git commit -m 'CCC-4 added pipeline configuration yml' 
```

Jira will then associate the commit with the task issue and your branch with the story issue on the Scrum Board. You will be able to pull up the Jira board directly from your **BitBucket** repo branch and visa versa. There are tons of other features you can look into if you're interested, such as logging your time in Jira directly from your commit message, setting the workflow status (e.g., In Progress, To-Do, Done, Accepted) and lots more. 